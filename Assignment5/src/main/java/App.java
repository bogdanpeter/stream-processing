

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Hello world!
 *
 */
public class App 
{
	private static final String PATH = "F:\\PT2017_30423_PeterCatalinBogdan_Assigment_5\\Activities.txt";
	private File file;
	List<MonitoredData> data;
	App(){
		file = new File(PATH);
		data = new ArrayList<MonitoredData>();
	}
	public void readData(){
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(file));
			String text = null;
			String[] split = null;
			LocalDateTime startTime;
			LocalDateTime endTime;
			DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd' 'HH:mm:ss");
//			DateFormat format = new SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss");
			String activityLabel;
			MonitoredData newData;
			while ((text = reader.readLine()) != null) {
				split = text.split("	");
				startTime = LocalDateTime.parse(split[0], format);;
				endTime = LocalDateTime.parse(split[2], format);
				activityLabel = split[4];
				newData = new MonitoredData(startTime, endTime, activityLabel);
				data.add(newData);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public List<MonitoredData> getData(){
		return this.data;
	}
	
	public static void main( String[] args)
    {
    	App lambda = new App();
    	lambda.readData();
    	List<MonitoredData> data = lambda.getData();
    	
    	//TASK 1
    	long task1 = data.stream().
    			map(MonitoredData::getStartingDay).
    			distinct().
    			count();
    	System.out.println(task1);
    	try{
    	    PrintWriter writer = new PrintWriter("task1.txt", "UTF-8");
    	    writer.println(task1);
    	    writer.close();
    	} catch (IOException e) {
    	   e.printStackTrace();
    	}
    	//TASK 2 
    	Map<String, Long> task2 = data.stream().
    			collect(Collectors.groupingBy( MonitoredData::getActivityLabel,
    					Collectors.counting()));
    	for(String s : task2.keySet()) {
    		System.out.println(s + " " + task2.get(s));
    	}
    	try{
    	    PrintWriter writer = new PrintWriter("task2.txt", "UTF-8");
    	    for (String s :task2.keySet()) {
    	    	writer.println(s + " " + task2.get(s));
    	    }
    	    writer.close();
    	} catch (IOException e) {
    	   e.printStackTrace();
    	}

    	//TASK 3`
    	Map<Integer, Map<String,Long>> task3 = data.stream().
    			collect(Collectors.groupingBy( MonitoredData::getStartingDay,
    					Collectors.groupingBy(MonitoredData::getActivityLabel, Collectors.counting())));
    	for(int i : task3.keySet()) {
    		System.out.println(i + "  " + task3.get(i));
    	}
    	try{
    	    PrintWriter writer = new PrintWriter("task3.txt", "UTF-8");
    	    for(int i : task3.keySet()) {
        		writer.println(i - 331+ "  " + task3.get(i));
        	}
    	    writer.close();
    	} catch (IOException e) {
    	   e.printStackTrace();
    	}

    	//TASK 4
		Map<String, Long> task4 = data.stream().
    			collect(Collectors.groupingBy(MonitoredData::getActivityLabel, 
    					Collectors.summingLong(MonitoredData::getDuration))).
    					entrySet().stream().
    					filter(e -> e.getValue() > 36000).
    					collect(Collectors.toMap(p -> p.getKey(), v-> v.getValue()));
		try{
    	    PrintWriter writer = new PrintWriter("task4.txt", "UTF-8");
    	    task4.forEach((k,v) -> {
    	    	long h = v / 3600;
    	    	long m =(v % 3600) / 60;
    	    	long s = (v% 60);
    	    	writer.println(k + " " + h + ":" + m + ":" + s );
    	    });
    	    writer.close();
    	} catch (IOException e) {
    	   e.printStackTrace();
    	}
		// TASK 5
		List<String> task5 = data.stream().
				filter(t -> t.getDuration() < 5 * 60).
				collect(Collectors.groupingBy(MonitoredData::getActivityLabel, Collectors.counting())).
				entrySet().
				stream().map(temp->{
					String toRet = temp.getKey();
					long occurence = task2.get(toRet);
//					System.out.println(toRet + "   " + occurence + "   " + temp.getValue());
					if ((temp.getValue() / occurence) > 0.9) {
						return toRet;
					}
					return null;
				}).collect(Collectors.toList());
		task5.removeAll(Collections.singleton(null));
		try{
    	    PrintWriter writer = new PrintWriter("task5.txt", "UTF-8");
    	    for (String s : task5) {
    	    	writer.println(s);
    	    }
    	    writer.close();
    	} catch (IOException e) {
    	   e.printStackTrace();
    	}
    }
}
