import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Date;

public class MonitoredData {
	private LocalDateTime startTime;
	private LocalDateTime endTime;
	private String activityLabel;
	
	MonitoredData(){
		this.activityLabel = "";
	}
	MonitoredData(LocalDateTime startTime, LocalDateTime endTime, String activityLabel){
		this.startTime = startTime;
		this.endTime = endTime;
		this.activityLabel = activityLabel;
	}
	public LocalDateTime getStartTime(){
		return this.startTime;
	}
	public void setStartTime(LocalDateTime startTime){
		this.startTime = startTime;
	}
	public LocalDateTime getEndTime(){
		return this.endTime;
	}
	public void setEndTime(LocalDateTime endTime){
		this.endTime = endTime;
	}
	public String getActivityLabel(){
		return this.activityLabel;
	}
	public void setActivityLabel(String activityLabel){
		this.activityLabel = activityLabel;
	}
	public int getStartingDay() {
		return this.startTime.getDayOfYear();
	}
	public int getEndDay() {
		return this.endTime.getDayOfYear();
	}
	public long getDuration() {
		LocalDateTime temp = LocalDateTime.from(startTime);
		long seconds = temp.until(endTime, ChronoUnit.SECONDS);
		return seconds;
	}
	public String toString(){
		String toRet = startTime + " 	" + endTime + "		" + activityLabel;
		return toRet;
	}
}
